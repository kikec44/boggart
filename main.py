from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import os

# print(os.path.dirname())
print(os.getcwd())
driver = webdriver.Chrome(os.getcwd()+"\\Drivers\\chromedriver.exe")
driver.get("http://www.python.org")
driver.save_screenshot(os.getcwd()+"\\Evidence\\image.png")
assert "Python" in driver.title
elem = driver.find_element_by_name("q")
elem.clear()
elem.send_keys("pycon")
elem.send_keys(Keys.RETURN)
assert "No results found." not in driver.page_source
driver.close()
